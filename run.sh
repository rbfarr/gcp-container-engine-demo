#!/bin/bash

set -e

JAVA_OPTS=${JAVA_OPTS:="-Xmx256m"}

exec java -jar $JAVA_OPTS gcp-container-engine-demo-1.0.0.jar

