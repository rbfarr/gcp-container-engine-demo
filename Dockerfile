FROM anapsix/alpine-java

ADD target/gcp-container-engine-demo-1.0.0.jar .
ADD run.sh /

RUN chmod 755 /run.sh

EXPOSE 8080
ENTRYPOINT ["/run.sh"]

